//
//
//
#pragma once
#ifndef BANKINGSYSTEM_SAVINGSACCOUNT_H
#define BANKINGSYSTEM_SAVINGSACCOUNT_H

#include "Account.h"
#include <iostream>

class SavingsAccount : public Account {

public:
    bool status = true;

public:
    SavingsAccount(double bal, float intRate);
    void withdraw(double withdrawalAmt);
    void deposit(double depositAmount);
    void monthlyProc();
};

#endif //BANKINGSYSTEM_SAVINGSACCOUNT_H
