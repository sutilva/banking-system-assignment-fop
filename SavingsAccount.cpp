using namespace std;

#include "SavingsAccount.h"
#include "Account.h"

SavingsAccount::SavingsAccount(double bal, float intRate) : Account(bal, intRate) {
};

void SavingsAccount::withdraw(double withdrawalAmt) {
    if (balance < 25) {
        status = false;
        cout << "(Account has been Deactivated due to Balance below 25)" << endl;
    } else {
        Account::withdraw(withdrawalAmt);
    }
}

void SavingsAccount::deposit(double depositAmount) {
    if (status == false && depositAmount > 25) {
        status = true;
    }
    Account::deposit(depositAmount);
}

void SavingsAccount::monthlyProc() {
    if (withdrawalCount > 4) {
        serviceCharge++;
    }
    Account::monthlyProc();
    if (balance < 25) {
        status = false;
    }
}