using namespace std;

#include "Account.h"
#include "CheckingAccount.h"

CheckingAccount::CheckingAccount(double bal, float intRate) : Account(bal, intRate) {
};

void CheckingAccount::withdraw(double withdrawalAmt) {
    if (balance < withdrawalAmt) {
        balance = balance - 15;
        checkWrittenWithdrawal++;
        cout << "(Insufficient Balance, $15 charge has been incurred)" << endl;
    } else {
        Account::withdraw(withdrawalAmt);
        checkWrittenWithdrawal++;
    }
}

void CheckingAccount::monthlyProc() {
    serviceCharge = serviceCharge + checkWrittenWithdrawal * 0.10 + 5;
    Account::monthlyProc();
}