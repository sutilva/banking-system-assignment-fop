#include <iostream>

using namespace std;

#include "SavingsAccount.h"
#include "CheckingAccount.h"

void savingsDepositTest();

void savingsWithdrawalTest();

void savingsStatusTest();

void checkingDepositTest();

void checkingWithdrawalTest();

void checkingInsufficientBalanceTest();

int main() {
    cout << " ----------- TEST CASES ------------" << endl;
    savingsDepositTest();
    savingsWithdrawalTest();
    savingsStatusTest();
    checkingDepositTest();
    checkingWithdrawalTest();
    checkingInsufficientBalanceTest();

    SavingsAccount savingsAccount(100, 5);
    CheckingAccount checkingAccount(200, 2);

    int choice;

    do {
        cout << endl << "Bank Account Menu: " << endl <<
             "1. Savings account deposits" << endl <<
             "2. Savings account withdraw" << endl <<
             "3. Checking account deposits" << endl <<
             "4. Checking account withdraw" << endl <<
             "5. Monthly statistics calculation" << endl <<
             "6. statistics Point" << endl <<
             "7. Exit " << endl << endl <<
             "Please enter your choice: " << endl;

        cin >> choice;

        switch (choice) {
            case 1:
                double depositIn;
                cout<<"(Curr. Balance $"<<savingsAccount.balance<<") Please enter amount to deposit: "<<endl;
                cin>>depositIn;
                savingsAccount.deposit(depositIn);
                cout<<"New Balance: $"<<savingsAccount.balance << endl;
                break;
            case 2:
                double withdrawIn;
                cout<<"(Curr. Balance $"<<savingsAccount.balance<<") Please enter amount to Withdraw: "<<endl;
                cin>>withdrawIn;
                savingsAccount.withdraw(withdrawIn);
                cout<<"New Balance: $"<<savingsAccount.balance << endl;
                break;
            case 3:
                double depositCheckIn;
                cout<<"(Curr. Balance $"<<checkingAccount.balance<<") Please enter amount to deposit: "<<endl;
                cin>>depositCheckIn;
                checkingAccount.deposit(depositCheckIn);
                cout<<"New Balance: $"<<checkingAccount.balance << endl;
                break;
            case 4:
                double withdrawCheckIn;
                cout<<"(Curr. Balance $"<<checkingAccount.balance<<") Please enter amount to Withdraw: "<<endl;
                cin>>withdrawCheckIn;
                checkingAccount.withdraw(withdrawCheckIn);
                cout<<"New Balance: $"<<checkingAccount.balance << endl;
                break;
            case 5:
                cout << "--- Monthly Summary of Savings ---" << endl;
                savingsAccount.monthlyProc();
                cout << "Monthly Interest Amount: $"<<savingsAccount.monthlyInterestAmt <<endl;
                cout<<"Account Balance: $"<<savingsAccount.balance << endl;
                cout << "--- Monthly summary of Checking ---" << endl;
                checkingAccount.monthlyProc();
                cout << "Monthly Interest Amount: $"<<checkingAccount.monthlyInterestAmt <<endl;
                cout<<"Account Balance: $"<<checkingAccount.balance << endl;
                break;
            case 6:
                cout << "------ SAVINGS ACCOUNT ------" << endl;
                cout << "Account Balance: $" << savingsAccount.balance << endl;
                cout << "Monthly Number of Deposits: " << savingsAccount.monthlyDepositsCount << endl;
                cout << "Number of Withdrawals: " << savingsAccount.withdrawalCount << endl;
                cout << "Service Charges: $" << savingsAccount.serviceCharge << endl;
                cout << "Annual Interest Rate: " << savingsAccount.annualInterestRate << endl;
                cout << "Status: "; if(savingsAccount.status) {cout<<"ACTIVE";} else {cout<<"NOT ACTIVE";}
                cout << endl<<endl;
                cout << "------ CHECKING ACCOUNT ------" << endl;
                cout << "Account Balance: $" << checkingAccount.balance << endl;
                cout << "Monthly Number of Deposits: " << checkingAccount.monthlyDepositsCount << endl;
                cout << "Number of Withdrawals: " << checkingAccount.withdrawalCount << endl;
                cout << "Service Charges: $" << checkingAccount.serviceCharge << endl;
                cout << "Annual Interest Rate: " << checkingAccount.annualInterestRate << endl;
                cout << endl;
                break;
            case 7:
                exit(0);
        }
    } while (choice != 7);

    return 0;
}

void savingsDepositTest() {
    SavingsAccount savingsAccountTest(50, 5);
    savingsAccountTest.deposit(10);
    savingsAccountTest.deposit(25);
    if (savingsAccountTest.balance == 85 && savingsAccountTest.monthlyDepositsCount == 2) {
        cout << "Deposit in Savings Account Test : PASSED" << endl;
    } else {
        cout << "Deposit in Savings Account Test : FAILED" << endl;
    }
}

void savingsWithdrawalTest() {
    SavingsAccount savingsAccountTest(50, 5);
    savingsAccountTest.withdraw(15);
    if (savingsAccountTest.balance == 35 && savingsAccountTest.withdrawalCount == 1) {
        cout << "Withdrawal in Savings Account Test : PASSED" << endl;
    } else {
        cout << "Withdrawal in Savings Account Test : FAILED" << endl;
    }
};

void savingsStatusTest() {
    SavingsAccount savingsAccountTest(50, 5);
    savingsAccountTest.withdraw(35);
    savingsAccountTest.withdraw(10);
    if (savingsAccountTest.balance == 15 && savingsAccountTest.status == false) {
        cout << "Status of Savings Account Test : PASSED" << endl;
    } else {
        cout << "Status of Savings Account Test : FAILED" << endl;
    }
};

void checkingDepositTest() {
    CheckingAccount checkingAccountTest(80, 2);
    checkingAccountTest.deposit(12);
    if (checkingAccountTest.balance == 92 && checkingAccountTest.monthlyDepositsCount == 1) {
        cout << "Deposit in Checking Account Test : PASSED" << endl;
    } else {
        cout << "Deposit in Checking Account Test : FAILED" << endl;
    }
};

void checkingWithdrawalTest() {
    CheckingAccount checkingAccountTest(80, 2);
    checkingAccountTest.withdraw(40);
    if (checkingAccountTest.balance == 40 && checkingAccountTest.withdrawalCount == 1) {
        cout << "Withdrawal in Checking Account Test : PASSED" << endl;
    } else {
        cout << "Withdrawal in Checking Account Test : FAILED" << endl;
    }
};

void checkingInsufficientBalanceTest() {
    CheckingAccount checkingAccountTest(85, 2);
    checkingAccountTest.withdraw(100);
    if (checkingAccountTest.balance == 70 && checkingAccountTest.checkWrittenWithdrawal == 1) {
        cout << "Insufficient Balance in Checking Account Test : PASSED" << endl;
        cout << "Check written withdrawal in Checking Account Test : PASSED" << endl;
    } else {
        cout << "Insufficient Balance in Checking Account Test : FAILED" << endl;
    }
};
