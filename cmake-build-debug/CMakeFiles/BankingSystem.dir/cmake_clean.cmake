file(REMOVE_RECURSE
  "BankingSystem.exe"
  "BankingSystem.exe.manifest"
  "BankingSystem.lib"
  "BankingSystem.pdb"
  "CMakeFiles/BankingSystem.dir/Account.cpp.obj"
  "CMakeFiles/BankingSystem.dir/CheckingAccount.cpp.obj"
  "CMakeFiles/BankingSystem.dir/SavingsAccount.cpp.obj"
  "CMakeFiles/BankingSystem.dir/main.cpp.obj"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/BankingSystem.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
