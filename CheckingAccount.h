//
//
//

#ifndef BANKINGSYSTEM_CHECKINGACCOUNT_H
#define BANKINGSYSTEM_CHECKINGACCOUNT_H

#include "Account.h"
#include <iostream>

class CheckingAccount : public Account{

public:
    int checkWrittenWithdrawal=0;

public:
    CheckingAccount(double bal, float intRate);
    void withdraw(double withdrawalAmt);
    void monthlyProc();
};


#endif //BANKINGSYSTEM_CHECKINGACCOUNT_H
