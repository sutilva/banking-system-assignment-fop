//
//
//

#ifndef BANKINGSYSTEM_ACCOUNT_H
#define BANKINGSYSTEM_ACCOUNT_H

class Account
{
public:
    double balance;
    int monthlyDepositsCount = 0;
    int withdrawalCount = 0;
    float annualInterestRate;
    float monthlyInterestRate;
    float serviceCharge = 0;
    double withdrawalAmount;
    double monthlyInterestAmt;

public:
    Account(double bal, float intRate);
    virtual void deposit(double depositAmount);
    virtual void withdraw(double withdrawalAmt);
    virtual void calcInt();
    virtual void monthlyProc();
};

#endif //BANKINGSYSTEM_ACCOUNT_H
