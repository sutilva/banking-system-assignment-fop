using namespace std;

#include <iostream>
#include "Account.h"

Account::Account(double bal, float intRate) {
    balance = bal;
    annualInterestRate = intRate;
};

void Account::deposit(double depositAmount) {
    balance = balance + depositAmount;
    monthlyDepositsCount++;
}

void Account::withdraw(double withdrawalAmt) {
    balance = balance - withdrawalAmt;
    withdrawalCount++;
}

void Account::calcInt() {
    monthlyInterestRate = (annualInterestRate / 12);
    monthlyInterestAmt = balance * monthlyInterestRate;
    balance = balance + monthlyInterestAmt;
}

void Account::monthlyProc() {
    balance = balance - serviceCharge;
    Account::calcInt();
    cout << "Monthly Service Charge: $"<<serviceCharge <<endl;
    serviceCharge = 0;
    withdrawalCount = 0;
    monthlyDepositsCount = 0;
}